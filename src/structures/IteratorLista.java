package structures;

import java.util.Iterator;

public class IteratorLista<T> implements Iterator
{

	private NodoLista<T> proximo; 

	private NodoLista<T> ant_prox; 

	public IteratorLista(NodoLista<T> primero)
	{
		proximo = primero; 
	}

	public boolean hasNext() {
		// TODO Auto-generated method stub
		return proximo != null; 
	}

	public T next() 
	{
		// TODO Auto-generated method stub
		T retornar = proximo.darElemento();
		ant_prox = proximo;
		proximo = proximo.darSiguiente(); 
		return retornar; 
	}
}
