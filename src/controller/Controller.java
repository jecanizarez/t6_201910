package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.SortedMap;
import java.util.concurrent.ThreadLocalRandom;

import javax.swing.plaf.synth.SynthScrollBarUI;

import org.omg.CosNaming.NamingContextExtPackage.AddressHelper;

import com.google.gson.*;

import model.util.Sort;
import mundo.*;
import structures.ArregloDinamico;
import structures.LinearHash;
import structures.SeparateChainingHash;
import view.MovingViolationsManagerView;

public class Controller 
{

	private enum Meses
	{
		January(0), February(0), March(0), April(0), May(0), June(0);
		private int infracciones;

		private Meses(int cantidad)
		{ 
			this.infracciones = cantidad;
		}

		private void contar()
		{ 
			this.infracciones++; 
		}

		private int darInfracciones()
		{ 
			return infracciones; 
		}
	}
	private MovingViolationsManagerView view;
	private ArregloDinamico<VOMovingViolations> datos;
	VOMovingViolations [] muestra;
	public Controller() 
	{
		view = new MovingViolationsManagerView();
		datos=new ArregloDinamico<VOMovingViolations>(300000);
	}

	public void run() 
	{
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		Controller controller = new Controller();
		while(!fin)
		{
			view.printMenu();
			int option = sc.nextInt();

			switch(option)
			{
			case 0:
				this.loadMovingViolations();
				break;
			case 1:

				view.printMessage("Ingrese el addressID del cual desea obtener las infracciones con accidente");

				int address_id=sc.nextInt();

				ArregloDinamico<VOMovingViolations> z= this.infraccionesConAccidente().get(address_id);
				System.out.println("Se encontraron "+z.darTamano()+" infracciones que terminaron en accidente con el AddressID indicado:");
				if(z!=null)
				{
					VOMovingViolations[] violaciones=new VOMovingViolations[z.darTamano()];
					for(int i=0;i<z.darTamano();i++)
					{
						violaciones[i]=z.darElemento(i);
					}
					Sort.sortM(violaciones, new VOMovingViolations.comparatorDate());
					for(int i=0;i<violaciones.length;i++)
					{
						if(violaciones[i]!=null)
							view.printMessage(violaciones[i].toString());
						else break;
					}
				}
				else 
					view.printMessage("El codigo no es v�lido o no existen infracciones asociadas a la direcci�n ingresada");
				break;

			case 2: 

				view.printMessage("Ingrese el addressID del cual desea obtener las infracciones con accidente");
				int address = sc.nextInt(); 
				SeparateChainingHash<Integer, ArregloDinamico<VOMovingViolations>> respuesta = this.infraccionesConAccidenteSeparateChaining();
				ArregloDinamico<VOMovingViolations> arreglo= respuesta.get(address);
				System.out.println("Se encontraron "+arreglo.darTamano()+" infracciones que terminaron en accidente con el AddressID indicado:");
				if(arreglo!=null)
				{
					VOMovingViolations[] infracciones = new VOMovingViolations[arreglo.darTamano()];
					for(int i = 0; i < arreglo.darTamano(); i++)
					{
						infracciones[i] = arreglo.darElemento(i);
					}
					Sort.sortM(infracciones, new VOMovingViolations.comparatorDate());
					for(VOMovingViolations e: infracciones)
					{

						System.out.println(e.toString());

					}
				}
				else 
					view.printMessage("El codigo no es v�lido o no existen infracciones asociadas a la direcci�n ingresada");

				break;
			case 3:
				sc.close();
				fin=true;
			}
		}
	}

	public void loadMovingViolations() 
	{
		try
		{
			for(Meses meses : Meses.values())
			{ 
				Gson gson=new Gson();
				BufferedReader br = new BufferedReader(new FileReader("./data/Moving_Violations_Issued_in_"+meses+"_2018.json"));
				VOMovingViolations[] actual =  gson.fromJson(br, VOMovingViolations[].class);
				for (int i = 0; i < actual.length; i++) 
				{
					datos.agregar(actual[i]);
					meses.contar();	
				}
				view.printMessage("Numero de infracciones en el mes de " + (meses.name()) + ": " + meses.darInfracciones());
			}	
		}
		catch(Exception e)
		{ 
			e.printStackTrace(); 
		}

		muestra=new VOMovingViolations[datos.darTamano()];
		for(int i  = 0; i < datos.darTamano(); i++)
		{
			muestra[i] = datos.darElemento(i);
		}
		Sort.sortM(muestra, new VOMovingViolations.comparadorAddressID());
		view.printMessage("Se encontraron "+datos.darTamano()+" MovingViolations");
	}

	public LinearHash<Integer,ArregloDinamico <VOMovingViolations>> infraccionesConAccidente()
	{
		LinearHash <Integer,ArregloDinamico<VOMovingViolations>> retorno= new LinearHash<Integer,ArregloDinamico<VOMovingViolations>>(20000);
		ArregloDinamico <VOMovingViolations> aux=new ArregloDinamico<VOMovingViolations>(200);
		int address_id=muestra[0].getAddressID();

		for(int i=0;i<muestra.length;i++)
		{
			if(muestra[i].getAddressID()==address_id && muestra[i].accident())
			{
				aux.agregar(muestra[i]);
			}
			else if(muestra[i].getAddressID()!=address_id)
			{
				retorno.put(address_id,aux);
				address_id=muestra[i].getAddressID();
				aux=new ArregloDinamico<VOMovingViolations>(200);
				if(muestra[i].accident())
					aux.agregar(muestra[i]);
			}
		}
		retorno.put(address_id,aux);

		return retorno;
	}

	public SeparateChainingHash<Integer, ArregloDinamico<VOMovingViolations>> infraccionesConAccidenteSeparateChaining()
	{
		SeparateChainingHash<Integer, ArregloDinamico<VOMovingViolations>> retorno = new SeparateChainingHash<>(20000);
		int addressID = muestra[0].getAddressID();
		ArregloDinamico<VOMovingViolations> arreglo = new ArregloDinamico<>(2);
		for(int j = 0; j < muestra.length; j++)
		{
			if(muestra[j].getAddressID() == addressID && muestra[j].accident())
			{
				arreglo.agregar(muestra[j]);
			}
			else if(muestra[j].getAddressID()!=addressID)
			{
				retorno.put(addressID, arreglo);
				addressID = muestra[j].getAddressID();
				arreglo = new ArregloDinamico<>(2);
				if(muestra[j].accident())
					arreglo.agregar(muestra[j]);
			}
		}
		retorno.put(addressID, arreglo);
		return retorno;
	}
}
