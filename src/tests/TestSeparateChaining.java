package tests;

import junit.framework.TestCase;
import structures.SeparateChainingHash;

public class TestSeparateChaining extends TestCase{
	private 	SeparateChainingHash<Integer, Integer> prueba;

	public void setUpEscenario()
	{
		prueba = new SeparateChainingHash<>(200);
		System.out.println("Tama�o inicial de la tabla: " + prueba.getCapacidad());

		for(int i = 0; i < 10000; i++)
		{
			prueba.put(i, i*2);
		}
		System.out.println("Tama�o final de la tabla: " + prueba.getCapacidad());
		System.out.println("Numero de tuplas: " + prueba.getSize());
		System.out.println("Numero de rehashes: " + prueba.getRehashes());
		System.out.println("Factor de carga: " + prueba.getSize()/prueba.getCapacidad());

	}
	public void testConsultas()
	{
		setUpEscenario();
		long startTime = System.nanoTime();
		for(int i = 0 ; i<10000; i++)
		{
			prueba.get(i);
		}
		long endTime = System.nanoTime() - startTime;
		long promedio = endTime/prueba.getSize();
		System.out.println("Tiempo promedio: "+ promedio + "ns");
	}


}
