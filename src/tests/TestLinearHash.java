package tests;

import junit.framework.TestCase;
import structures.*;

public class TestLinearHash extends TestCase
{
	LinearHash<String, String> tablaLinear;

	public void setUp()
	{
		tablaLinear= new LinearHash<String, String>(200);
		System.out.println("numero parejas inicial: "+tablaLinear.size());
		System.out.println("Tama�o inicial del arreglo: 200");
		for(int i=0;i<10000;i++)
		{
			tablaLinear.put("Prueba"+i,"Hola"+i*10);
		}
		assertEquals(10000,tablaLinear.size());
		System.out.println("tama�o final del arreglo: "+tablaLinear.size());
		System.out.println("Capacidad final del arreglo: "+tablaLinear.darCapacidad());
		System.out.println("Factor de carga: "+ tablaLinear.darFactorDeCargaActual());
		System.out.println("Cantidad rehashes: "+ tablaLinear.darRehashes());
	}

	public void testGetLinear()
	{
		long startTime = System.nanoTime();
		for(int i = 0; i < tablaLinear.size(); i++)
		{
			tablaLinear.get("Prueba"+i); 
		}
		assertEquals("Hola100",tablaLinear.get("Prueba10"));
		long endTime = System.nanoTime() - startTime;
		long promedio = endTime/tablaLinear.size();
		System.out.println("Tiempo promedio: "+ promedio+"ns");
		System.out.println("Numero de parejas final: "+ tablaLinear.darParejas());

	}
}