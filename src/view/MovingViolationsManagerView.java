package view;

import mundo.VOMovingViolations;
import structures.LinearHash;

public class MovingViolationsManagerView 
{
	/**
	 * Constante con el nÃºmero maximo de datos maximo que se deben imprimir en consola
	 */
	public static final int N = 20;

	public void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 6----------------------");
		System.out.println("0. Cargar datos");
		System.out.println("1. A�adir datos en Linear Hash Table y buscar infracciones por AddressId");
		System.out.println("2. A�adir datos en Separate Chaining Hash Table y buscar infracciones por AddressId");
		System.out.println("3.Salir");
	}

	public void printMessage(String mensaje) 
	{
		System.out.println(mensaje);
	}
}
